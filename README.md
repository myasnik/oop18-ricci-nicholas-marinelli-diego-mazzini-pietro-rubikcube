# Rubik Cube

## Instructions
1. Download and install **Java SE Runtime Environment 8** from [here](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) if you don't already have it
2. Download **rubikcube.jar** from this repository's downloads section
3. Now you can execute **rubikcube.jar** by either double clicking it or running the following command: `$ java -jar rubikcube.jar`

## How to play?
* Random Cube - A random Rubik Cube is generated and the user has to solve it.
	* Controls: 
		* Left-drag the cube to rotate it around his center.
		* Right-click centers and select with the context menu the direction of rotation.
* Solve Cube - The user is prompted to input each cube color (the user should have a "real" Rubik Cube that can't solve) and the application will solve the cube for the user.
	* Controls:
		* Left-drag the cube to rotate it around his center.
		* Right-click cubes faces and select with the context menu their color.
		* Enter to confirm the input of the whole Rubik Cube.
		* Left/Right arrow to trigger the previous/next move of the cube.

## Authors
* [Ricci Nicholas](https://bitbucket.org/Piccio98/)
* [Marinelli Diego](https://bitbucket.org/DEDEmarinelli/)
* [Mazzini Pietro](https://bitbucket.org/myasnik/)
