package movestructure;
/**
 * Enumerator used by {@link MoveUtils} and other visual classes.
 */
public enum Direction {
    /**
     * Right rotation of a {@link cubestructure.Cube3X3} {@link SideUtils} .
     */
    RIGHT,
    /**
     * Left rotation of a {@link cubestructure.Cube3X3} {@link SideUtils} .
     */
    LEFT
}
