package algorithms;

import cubestructure.Cube3X3;

/**
 * Class for tests.
 * 
 */
public class TestsUtils {
    /**
     * Automatic test.
     */

    public TestsUtils() {
        final Cube3X3 rubik = new Cube3X3();
        for (int i = 0; i < 1000; i++) {
            rubik.setRandomCube();
            final GeneralAlgorithm resolver = new GeneralAlgorithm(rubik);
            if (!GeneralAlgorithm.cubeVerify(rubik.getRubikCube())) {
                System.out.println("errore");
            }
        }
    }

}
